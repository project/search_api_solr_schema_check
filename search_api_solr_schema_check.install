<?php


/**
 * @file
 * xx
 */

use Drupal\search_api_solr\Controller\SolrConfigSetController;
use Drupal\search_api_solr\SearchApiSolrException;
use Drupal\search_api_solr\Utility\Utility as SearchApiSolrUtility;


/**
 * Implements hook_requirements().
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 * @throws \Drupal\search_api\SearchApiException
*/
function search_api_solr_schema_check_requirements($phase) {
  $requirements = [];

  if ($phase === 'runtime') {
    $servers = search_api_solr_get_servers();
    $module_extension_list = \Drupal::service('extension.list.module');
    foreach ($servers as $server_id => $server) {
      // Do we want to check this one?
      $site_config = \Drupal::service('config.factory')
        ->getEditable('search_api.server.' . $server_id);
      $backend_config = $site_config->get('backend_config');
      if (!empty($backend_config['repo_path'])) {
        // Is the repo path absolute or relative to the document root?
        if (substr($backend_config['repo_path'], 0, 1) === '/') {
          $repo_path = $backend_config['repo_path'];
        }
        else {
          $repo_path = DRUPAL_ROOT . '/' . $backend_config['repo_path'];
        }
        if ($server->status()) {
          /** @var \Drupal\search_api_solr\SolrBackendInterface $backend */
          $backend = $server->getBackend();
          $connector = $backend->getSolrConnector();
          if ($backend->isAvailable() && $connector->pingCore() && !$backend->isNonDrupalOrOutdatedConfigSetAllowed()) {
            $config_set_controller = new SolrConfigSetController($module_extension_list);
            $config_set_controller->setServer($server);
            $filenames_in_dir = [];
            $file_contents_in_dir = [];
            if ($handle = opendir($repo_path)) {
              while (FALSE !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                  $filenames_in_dir[] = $entry;
                }
              }
              closedir($handle);
            }
            foreach ($filenames_in_dir as $filename_in_dir) {
              if (!is_dir($repo_path . '/' . $filename_in_dir)) {
                $file_contents_in_dir[$filename_in_dir] = file_get_contents($repo_path . '/' . $filename_in_dir);
              }
            }
            $new_config_set = $file_contents_in_dir;
            if (!empty($new_config_set)) {
              try {
                $server_files_list = SearchApiSolrUtility::getServerFiles($server);
              }
              catch (SearchApiSolrException $e) {
                $server_files_list = [];
              }
              // The files that are already on the server.
              $server_file_names = array_keys($server_files_list);
              $new_config_file_names = array_keys($new_config_set);
              $extra_release_files = array_diff($new_config_file_names, $server_file_names);
              if (!empty($extra_release_files)) {
                $requirements['search_api_solr_schema_' . $server_id . '_modifications']['title'] = t('Solr Server %server', ['%server' => $server->label()]);
                $requirements['search_api_solr_schema_' . $server_id . '_modifications']['value'] = t('Schema incomplete');
                $requirements['search_api_solr_schema_' . $server_id . '_modifications']['severity'] = REQUIREMENT_WARNING;
                $requirements['search_api_solr_schema_' . $server_id . '_modifications']['description'] = t(
                  'There are some files missing in the Solr server schema <a href=":url">@server</a>: @files. An updated config.zip should be downloaded and deployed to your Solr server.', [
                    ':url' => $server->toUrl('canonical')->toString(),
                    '@server' => $server->label(),
                    '@files' => implode(', ', $extra_release_files),
                  ]
                );
              }
              foreach ($new_config_set as $new_file_name => $new_file_body) {
                if (stripos(strrev($new_file_name), 'lmx.') === 0) {
                  try {
                    $server_file_data = $connector->getFile($new_file_name);
                    $server_file_body = $server_file_data->getBody();
                  }
                  catch (SearchApiSolrException $e) {
                    $server_file_body = '';
                  }
                  [
                    $version_number_server,
                    $xml_server,
                  ] = SearchApiSolrUtility::normalizeXml($server_file_body);
                  [
                    $version_number_new,
                    $xml_new,
                  ] = SearchApiSolrUtility::normalizeXml($new_file_body);
                  if (strcmp($xml_server, $xml_new) !== 0) {
                    $requirements['search_api_solr_schema_' . $server_id . '_modifications_' . $new_file_name]['title'] = t('Solr Server %server', ['%server' => $server->label()]);
                    $requirements['search_api_solr_schema_' . $server_id . '_modifications_' . $new_file_name]['value'] = t('Schema not up to date');
                    $requirements['search_api_solr_schema_' . $server_id . '_modifications_' . $new_file_name]['severity'] = REQUIREMENT_ERROR;
                    $requirements['search_api_solr_schema_' . $server_id . '_modifications_' . $new_file_name]['description'] = t(
                      'There are some configuration differences between the active Solr server and the file in your repo. Server: <a href=":url">@server</a>. File: @filename.', [
                        ':url' => $server->toUrl('canonical')->toString(),
                        '@server' => $server->label(),
                        '@version' => $version_number_new,
                        '@filename' => $new_file_name,
                      ]
                    );
                  }
                  else {
                    $requirements['search_api_solr_schema_' . $server_id . '_modifications_' . $new_file_name]['title'] = t('Solr Server %server', ['%server' => $server->label()]);
                    $requirements['search_api_solr_schema_' . $server_id . '_modifications_' . $new_file_name]['value'] = t('Schema up to date');
                    $requirements['search_api_solr_schema_' . $server_id . '_modifications_' . $new_file_name]['severity'] = REQUIREMENT_OK;
                    $requirements['search_api_solr_schema_' . $server_id . '_modifications_' . $new_file_name]['description'] = t(
                      'There are no differences between the active Solr server and the file in your repo. Server: <a href=":url">@server</a>. File: @filename.', [
                        ':url' => $server->toUrl('canonical')->toString(),
                        '@server' => $server->label(),
                        '@version' => $version_number_new,
                        '@filename' => $new_file_name,
                      ]
                    );
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  return $requirements;
}
